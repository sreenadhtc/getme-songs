package com.imsreenadh.getmesongs;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

/**
 * Created by Sreenadh on 12-Dec-15.
 */
public class SongListAdapter extends RecyclerView.Adapter<SongListAdapter.SongListViewHolder> {

    private LayoutInflater inflater;
    List<SongListInformation> data = Collections.emptyList();

    public SongListAdapter(Context context, List<SongListInformation> data){
        inflater = LayoutInflater.from(context);
        this.data = data;
    }
    @Override
    public SongListViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.song_list_row,viewGroup,false);
        SongListViewHolder holder = new SongListViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(SongListViewHolder viewHolder, int position) {

        SongListInformation current = data.get(position);
        viewHolder.songTitle.setText(current.songName);
        viewHolder.songAuthor.setText(current.authorInfo);
        viewHolder.songSize.setText(current.songSize);
        viewHolder.albumName.setText(current.albumName);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    //*****************************************
    // now create view holder class and object
    //*****************************************
    class SongListViewHolder extends RecyclerView.ViewHolder{
        ImageView poster;
        TextView songTitle;
        TextView songAuthor;
        TextView songSize;
        TextView albumName;

         public SongListViewHolder(View itemView) {
            super(itemView);
             poster = (ImageView) itemView.findViewById(R.id.album_poster);
             songTitle = (TextView) itemView.findViewById(R.id.song_title);
             songAuthor = (TextView) itemView.findViewById(R.id.song_author);
             songSize = (TextView) itemView.findViewById(R.id.song_size);
             albumName = (TextView) itemView.findViewById(R.id.album_name);
        }
    }
}
