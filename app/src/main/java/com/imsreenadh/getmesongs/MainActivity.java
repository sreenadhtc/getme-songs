package com.imsreenadh.getmesongs;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
//JSoup for web scrapping


public class MainActivity extends AppCompatActivity {


    private Toolbar toolbar;
    private String search_ip;
    private SearchView searchView = null;
    private String[] href_links = new String[100]; // for storing all album links one by one and then reconnecting.
    private RecyclerView vSongList;
    private SongListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_appbar);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        vSongList = (RecyclerView) findViewById(R.id.song_list);
        /* adapter = new SongListAdapter(getApplicationContext(),getData());
        vSongList.setAdapter(adapter);
        vSongList.setLayoutManager(new LinearLayoutManager(getApplicationContext())); */

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        NavDrawer drawerFragment = (NavDrawer) getSupportFragmentManager().findFragmentById(R.id.nav_drawer);
        drawerFragment.setUp(R.id.nav_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
    }

    // ********************************************
    // building the list
    // ********************************************

    public static List<SongListInformation> getData() {
        List<SongListInformation> data = new ArrayList<>();
        String[] posterSrc = {"link", "", "", ""};
        String[] songName = {"Manwa Laage", "Behka ye Behka", "Varu Pokaam"};
        String[] albumName = {"Happy New Year", "Ghajini", "Rani Padmini"};
        String[] authorInfo = {"Shreya Ghosal", "Arijit Singh", "Sayanora"};
        String[] songSize = {"11.20MB", "9.07MB", "5.9MB", "6.78MB"};

        for (int i = 0; i < songName.length && i < albumName.length && i < authorInfo.length && i < songSize.length; i++) {
            SongListInformation current = new SongListInformation();
            current.posterSrc = posterSrc[i];
            current.songName = songName[i];
            current.songSize = songSize[i];
            current.authorInfo = authorInfo[i];
            current.albumName = albumName[i];
            data.add(current);
        }
        return data;
    }
    // *************************************************************************************
    // AsyncTask helps in running CPU intensive process in bg, away from UI thread
    // *************************************************************************************

    public class GetResultBg extends AsyncTask<String, Void, String> {

        //String result_links[][];
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.hide();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
            // ******************************************
            // on successful search output, display list
            // ******************************************

            adapter = new SongListAdapter(getApplicationContext(), getData());
            vSongList.setAdapter(adapter);
            vSongList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            progressDialog = ProgressDialog.show(MainActivity.this, "Doing Magic", "Loading...Please be Patient ☻", true);
        }

        @Override
        protected String doInBackground(String... params) {
            System.out.println("doInBackgournd...");
            String search_result_link = "";
            Document search_doc; //to save and use document of page after search query
            Elements searched_results = null;

            Elements final_result_links;
            Document result_link_doc;//to save and use document of page after selecting album(href)

            String parsed_hrefs[] = new String[100];
            String to_search = "http://www.maango.me/index.php?do=search&subaction=search&story=" + params[0];
            System.out.println("full link: " + to_search);

            int i = 0, j = 0; // to iterate through all album links
            // *************************************************
            // The code below is for searching all possibilities
            // *************************************************
            try {
                publishProgress();
                search_doc = Jsoup.connect(to_search).userAgent("Mozilla").timeout(14000).get();
                if (search_doc != null) {
                    searched_results = search_doc.select(".look-movie");// whole html content of class obtained, now get href --> tested ok(bug?)
                    System.out.println("Your result: " + searched_results.toString());
                    for (Element href : searched_results) {
                        search_result_link = href.attr("href");
                        System.out.println("inside 1st for : " + search_result_link); // href obtained --> tested ok(bug?)
                        href_links[i] = search_result_link;
                        System.out.println(i + " : " + href_links[i]);
                        if (href_links[i] != null) {
                            //connect to the result link of search
                            result_link_doc = Jsoup.connect(search_result_link).userAgent("Mozilla").timeout(14000).get();
                            if (result_link_doc != null) {
                                final_result_links = result_link_doc.select("div#song_link > a[title= Normal Download]");
                                //System.out.println("Your final result: " + final_result_links.toString());
                                for (Element final_href : final_result_links) {
                                    parsed_hrefs[j] = final_href.attr("href");
                                    System.out.println("inside 2nd for : " + parsed_hrefs[j]);// href obtained --> tested ok(bug?)

                                    j++;
                                }
                            } else {                                                    // --> force close issue not solved for invalid searches
                                Context context = getApplicationContext();              // this else doesn't work for now
                                int duration = Toast.LENGTH_SHORT;
                                Toast toast = Toast.makeText(context, "Nothing Found!!", duration);
                                toast.show();
                                return "0";
                            }
                        }
                        i++;
                    }
                }
            } catch (IOException e) {
                System.out.print("Exception in connect:" + e);
            }
            if (i == 1) {
                System.out.println("TOAST ME");
                Context context = getApplicationContext();              // this else doesn't work for now
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, "Nothing Found!! Try Again ☺", duration);
                toast.show();
            }
            return "";
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //Handling menu options
        switch (item.getItemId()) {
            case R.id.action_quit:
                new AlertDialog.Builder(this)
                        .setMessage("Do you really want to quit?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                MainActivity.this.finish();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
                return true;
            case R.id.action_settings:
                showSettings();
                return true;
            case R.id.action_about_gms:
                showAbout();
                return true;
            case R.id.action_refine_search:
                showRefine();
                //setContentView(R.layout.magic_search);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String newText) {
                // this is your adapter that will be filtered
                return true;
            }

            public boolean onQueryTextSubmit(String query) {
                //The value "query" which is entered in the search view box.
                search_ip = query;
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
                Context context = getApplicationContext();
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, search_ip, duration);
                try {
                    searchSong(search_ip.replace(' ', '+'));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                toast.show();
                return true;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);

        return super.onCreateOptionsMenu(menu);
    }

    private void showRefine() {
        toaster("Refine");
    }

    private void showAbout() {
        toaster("About");
    }

    private void showSettings() {
        toaster("Settings");
    }

    public void toaster(String s) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, s, duration);
        toast.show();
    }

    public boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    public void searchSong(String search_song_input) throws IOException {
        if (hasConnection()) {
            //perform search
            if (search_song_input.length() == 0) {
                Context context = getApplicationContext();
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, "Please enter a Song name!", duration);
                toast.show();
                // System.out.println("inside if, null entry check"); --> debug purpose,checked
            } else {
                new GetResultBg().execute(search_song_input);
            }
            // System.out.println("After validation: "+ search_input); --> debug purpose,checked
        } else {
            //alert the user using toast
            Context context = getApplicationContext();
            CharSequence text = "Sorry, you have no Internet access!!";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }
}
