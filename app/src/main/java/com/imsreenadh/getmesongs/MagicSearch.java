/*
    MagicSearch is created for handling the search algorithms and finding the right download link
                - Sreenadh TC

    MagicSearch also have a subclass, GetResultBg to that implements Background threads in which
    the searching and link parsing is done.
*/

package com.imsreenadh.getmesongs;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class MagicSearch extends AppCompatActivity {

    public class GetResultBg extends AsyncTask<String, Void, String> {

        //String result_links[][];

        @Override
        protected String doInBackground(String... params) {
            System.out.println("doInBackgournd...");
            String search_result_link = "";
            Document search_doc; //to save and use document of page after search query
            Elements searched_results;

            Elements final_result_links;
            Document result_link_doc;//to save and use document of page after selecting album(href)

            String parsed_hrefs;
            String to_search = "http://www.maango.me/index.php?do=search&subaction=search&story=" + params[0];
            System.out.println("full link: " + to_search);
            try {
                search_doc = Jsoup.connect(to_search).userAgent("Mozilla").timeout(14000).get();
                if (search_doc != null) {
                    searched_results = search_doc.select(".look-movie");// whole html content of class obtained, now get href --> tested ok(bug?)
                    System.out.println("Your result: " + searched_results.toString());

                    for (Element href : searched_results) {
                        search_result_link = href.attr("href");
                        System.out.println("inside for : " + search_result_link); // href obtained --> tested ok(bug?)
                    }
                    if (search_result_link != null) {
                        //connect to the result link of search
                        result_link_doc = Jsoup.connect(search_result_link).userAgent("Mozilla").timeout(14000).get();
                        if (result_link_doc != null) {
                            final_result_links = result_link_doc.select("div#song_link > a[title]");
                            System.out.println("Your final result: " + final_result_links.toString());

                            for (Element final_href : final_result_links) {
                                parsed_hrefs = final_href.attr("href");
                                System.out.println("inside for : " + parsed_hrefs); // href obtained --> tested ok(bug?)
                            }
                        } else {
                            return "0";
                        }
                    }
                } else {
                    return "0";
                }
            } catch (IOException e) {
                System.out.print("Exception in connect:" + e);
            }
            return "";
        }
    }

    /*private static String trim(String s, int width) {
        if (s.length() > width)
            return s.substring(0, width - 1) + ".";
        else
            return s;
        // trim() for html parsing
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_refine_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}